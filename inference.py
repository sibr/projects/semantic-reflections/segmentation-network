"""Run DeepLab-ResNet on a given image.

This script computes a segmentation mask for a given image.
"""

from __future__ import print_function
import glob
import argparse
from datetime import datetime
import os
import sys
import time
import shutil

from PIL import Image

import tensorflow as tf
import numpy as np

from deeplab_resnet import DeepLabResNetModel, ImageReader, decode_labels, prepare_label

IMG_MEAN = np.array((120.2175,121.2017,119.0497), dtype=np.float32)

NUM_CLASSES = 4

SAVE_DIR = './output/'

class InferenceDataloader(object):
    """Inference dataloader"""
    def __init__(self, filenames_file):
        self.input_images = None

        with tf.variable_scope("inference_inputloader"):
            input_queue = tf.train.string_input_producer([filenames_file], capacity = 1, shuffle = False)
            line_reader = tf.TextLineReader()
            _, line = line_reader.read(input_queue)
            split_line = tf.string_split([line]).values

            img = tf.image.decode_jpeg(tf.read_file(split_line[0]))
            img_r, img_g, img_b = tf.split(axis=2, num_or_size_splits=3, value=img)
            img = tf.cast(tf.concat(axis=2, values=[img_b, img_g, img_r]), dtype=tf.float32)
            img -= IMG_MEAN 

            capacity = 1
            num_threads = 1
            self.input_images = tf.train.batch([img], 1, num_threads, capacity, dynamic_pad=True)


def get_arguments():
    """Parse all the arguments provided from the CLI.
    
    Returns:
      A list of parsed arguments.
    """
    parser = argparse.ArgumentParser(description="DeepLabLFOV Network Inference.")
    parser.add_argument("dataset_path", type=str,
                        help="Path to the dataset root directory.")
    parser.add_argument("--model_weights", type=str, default="./snapshots/model.ckpt-300000",
                        help="Path to the file with model weights.")
    parser.add_argument("--num-classes", type=int, default=NUM_CLASSES,
                        help="Number of classes to predict (including background).")
    return parser.parse_args()

def load(saver, sess, ckpt_path):
    '''Load trained weights.
    
    Args:
      saver: TensorFlow saver object.
      sess: TensorFlow session.
      ckpt_path: path to checkpoint file with parameters.
    ''' 
    saver.restore(sess, ckpt_path)
    print("Restored model parameters from {}".format(ckpt_path))

def main():
    """Create the model and start the evaluation process."""
    args = get_arguments()
    base_path = args.dataset_path
    ext = "jpg"
    images_files = [f for f in glob.glob(base_path + "/colmap/stereo/images/*.jpg", recursive=False)]
    if(len(images_files) == 0):
        images_files = [f for f in glob.glob(base_path + "/colmap/stereo/images/*.JPG", recursive=False)]
        ext = "JPG"
    if(len(images_files) == 0):
        images_files = [f for f in glob.glob(base_path + "/colmap/stereo/images/*.jpeg", recursive=False)]
        ext = "jpeg"
    if(len(images_files) == 0):
        images_files = [f for f in glob.glob(base_path + "/colmap/stereo/images/*.JPEG", recursive=False)]
        ext = "JPEG"
    if(len(images_files) == 0):
        images_files = [f for f in glob.glob(base_path + "/colmap/stereo/images/*.png", recursive=False)]
        ext = "png"
    if(len(images_files) == 0):
        images_files = [f for f in glob.glob(base_path + "/colmap/stereo/images/*.PNG", recursive=False)]
        ext = "PNG"

    

    resized_path = base_path + "/colmap/stereo/images_resized/"
    image_path = base_path + "/list.txt"
    save_path = base_path + "/semantic_reflections/masks/"

    if not os.path.exists(base_path + "/semantic_reflections/"):
        os.mkdir(base_path + "/semantic_reflections/")

    if not os.path.exists(save_path):
        os.mkdir(save_path)

    os.mkdir(resized_path)
    sizes = []
    # Save a downscaled copy of each image, and the path to this copy.
    with open(image_path, 'w+') as f:
        for file in images_files:
            im1 = Image.open(file)
            im2 = im1.resize((int(im1.size[0]/2), int(im1.size[1]/2)), Image.NEAREST)
            new_path = file.replace("/colmap/stereo/images", "/colmap/stereo/images_resized")
            new_path = new_path.replace(ext, "png")
            f.write(new_path + "\n")
            im2.save(new_path)
            sizes += [im1.size]
    

    # Create dataloader and network.
    dataloader = InferenceDataloader(image_path)
    net = DeepLabResNetModel({'data': dataloader.input_images}, is_training=False, num_classes=args.num_classes)

    # Which variables to load.
    restore_var = tf.global_variables()
    
    # Predictions.
    raw_output = net.layers['fc1_voc12']
    raw_output_up = tf.image.resize_bilinear(raw_output, tf.shape(dataloader.input_images)[1:3,])
    raw_output_up = tf.argmax(raw_output_up, dimension=3)
    pred = tf.expand_dims(raw_output_up, dim=3)
    
	# Set up TF session and initialize variables. 
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)
    init = tf.global_variables_initializer()
    sess.run(init)
	
    # Load weights.
    loader = tf.train.Saver(var_list=restore_var)
    load(loader, sess, args.model_weights)
    
    test_file = open(image_path, 'r')
    test_lines = test_file.readlines()
    test_file.close()
    
	# Start the data loader threads
    coordinator = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess, coord=coordinator)

    if not os.path.exists(save_path):
        os.makedirs(save_path)

    for i, line in enumerate(test_lines):
        if coordinator.should_stop():
            break
        
        # Perform inference.
        preds = sess.run(pred)
        msk = decode_labels(preds, num_classes=args.num_classes)
        im = Image.fromarray(msk[0])
        filename = os.path.basename(line)
        im1 = im.resize((sizes[i][0], sizes[i][1]), Image.NEAREST)
        im1.save(save_path + filename.strip()[:-4] + '.png')
        print('The output file has been saved to {}'.format(save_path + filename.strip()[:-4] + '.png'))

    os.remove(image_path)
    shutil.rmtree(resized_path)

if __name__ == '__main__':
    main()
